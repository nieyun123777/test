//
//  ChatCell.m
//  QQChat
//
//  Created by nieyun on 13-12-31.
//  Copyright (c) 2013年 dianfengkeji. All rights reserved.
//
#define MAXWIDTH    [UIScreen  mainScreen].bounds.size.width
#define MAXHEGHT   2000
#import "ChatCell.h"

@implementation ChatCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
       
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _chatLabel = [[UILabel  alloc]initWithFrame:CGRectZero];
        
       // self.chatLabel.backgroundColor = [UIColor colorWithRed:0.455 green:0.836 blue:1.000 alpha:1.000];
        
         self.chatLabel.font = [UIFont  systemFontOfSize:15];
        
        [self addSubview:_chatLabel];
        
        bgImagev = [[UIImageView  alloc]initWithFrame:CGRectZero];
        
        //bgImagev.backgroundColor = [UIColor  orangeColor];
        
    
        [self insertSubview:bgImagev atIndex:0];
        
    }
    return self;
}

- (void) countLenth:(NSString  *) text
{
    [self  imageStrech];
    UIFont  *font = [UIFont  systemFontOfSize:15];
    CGSize maxSize = CGSizeMake(MAXWIDTH, MAXHEGHT);
    NSString  *test = @"hehe";
    CGSize testSize = [test sizeWithFont:font constrainedToSize:maxSize];
    float height = testSize.height;
    CGSize  size ;
    CGSize  s = [text  sizeWithFont:font constrainedToSize:maxSize];
    if (s.height <= height  )
    {
        CGSize  size1 = CGSizeMake(MAXWIDTH, height );
          size = [text  sizeWithFont:font constrainedToSize:size1];
    
       
        
    }else
    {
        CGSize  size2 = CGSizeMake(MAXWIDTH, MAXHEGHT);
          size = [text  sizeWithFont:font constrainedToSize:size2];
        
        
        
            self.chatLabel.numberOfLines = 1000;
        
       
    }
     self.chatLabel.frame = CGRectMake(20, 5, size.width , size.height);
     bgImagev.frame = CGRectMake(0, 0, size.width + 30, size.height + 20);
    self.chatLabel.text =  text;
   
}
- (void) imageStrech
{
    UIImage  *image = [UIImage  imageNamed:@"chatfrom_bg_normal"];
    CGSize  imageSize = image.size;
    image = [image  stretchableImageWithLeftCapWidth: (image.size.width - 10)/2 + 10 topCapHeight:image.size.height/2];
    bgImagev.image = image;
    
}
- (void)layoutSubviews
{
    [self  countLenth:self.text ];
    //hehehe
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
