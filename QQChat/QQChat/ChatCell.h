//
//  ChatCell.h
//  QQChat
//
//  Created by nieyun on 13-12-31.
//  Copyright (c) 2013年 dianfengkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatCell : UITableViewCell
{
    UIImageView  *bgImagev;
}
@property (nonatomic,strong) UILabel  *chatLabel;
@property (nonatomic,copy) NSString  *text;
@end
