//
//  ViewController.m
//  QQChat
//
//  Created by nieyun on 13-12-31.
//  Copyright (c) 2013年 dianfengkeji. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:
        nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.chatTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // Do any additional setup after loading the view from its nib.
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize  size = CGSizeMake(320, 2000);
    NSLog(@"%@",self.uTextAr[indexPath.row]);
    NSString  *str = [self.uTextAr  objectAtIndex:indexPath.row];
    CGSize  s = [str  sizeWithFont:[UIFont  systemFontOfSize:15] constrainedToSize:size];
    NSLog(@"%f",s.height);
   
    return s.height + 20;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.uTextAr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static  NSString  *str = @"cell";
      ChatCell  *cell = [tableView  dequeueReusableCellWithIdentifier:str];
    if (cell == nil) {
        cell = [[ChatCell  alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    
    cell.text = [self.uTextAr  objectAtIndex:indexPath.row];
    return cell;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
