//
//  ViewController.h
//  QQChat
//
//  Created by nieyun on 13-12-31.
//  Copyright (c) 2013年 dianfengkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCell.h"

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *chatTable;
@property (nonatomic,strong) NSArray *uTextAr;
@property (nonatomic,strong) NSArray  *mTextAr;
@end
