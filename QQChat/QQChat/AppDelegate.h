//
//  AppDelegate.h
//  QQChat
//
//  Created by nieyun on 13-12-31.
//  Copyright (c) 2013年 dianfengkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
